let app;

window.onload = () => {

    const apiRes = {
        pieces: 15,
        values: [
            { value: 100 },
            { value: 200 },
            { value: 300 },
            { value: 400 },
            { value: 500 },
            { value: 600 },
            { value: 700 },
            { value: 800 },
            { value: 900 },
            { value: 1000 },
            { value: 1100 },
            { value: 1200 },
            { value: 1300 },
            { value: 1400 },
            { value: 1500 },
            { value: 1600 },

        ],
        winningValue: 1400
    }

    const spinAnimationTime = 3;

    const Application = PIXI.Application,
        Graphics = PIXI.Graphics,
        Container = PIXI.Container,
        Text = PIXI.Text,
        Sprite = PIXI.Sprite,
        TextStyle = PIXI.TextStyle;

    app = new Application({
        width: 1920,
        height: 1080,
        antialias: true,
    })
    app.renderer.backgroundColor = 0x790E2E;

    document.body.appendChild(app.view);

    const circleContainer = new Container();
    const spinBtnContainer = new Container();

    circleContainer.x = app.view.width / 2 - circleContainer.width / 2;
    circleContainer.y = app.view.height / 2 - circleContainer.height / 2;

    app.stage.addChild(circleContainer);
    app.stage.addChild(spinBtnContainer);

    var colorArray = ['0xFF6633', '0xFFB399', '0xFF33FF', '0xFFFF99', '0x00B3E6',
        '0xE6B333', '0x3366E6', '0x999966', '0x99FF99', '0xB34D4D',
        '0x80B300', '0x809900', '0xE6B3B3', '0x6680B3', '0x66991A',
        '0xFF99E6', '0xCCFF1A', '0xFF1A66', '0xE6331A', '0x33FFCC',
        '0x66994D', '0xB366CC', '0x4D8000', '0xB33300', '0xCC80CC',
        '0x66664D', '0x991AFF', '0xE666FF', '0x4DB3FF', '0x1AB399',
        '0xE666B3', '0x33991A', '0xCC9999', '0xB3B31A', '0x00E680',
        '0x4D8066', '0x809980', '0xE6FF80', '0x1AFF33', '0x999933',
        '0xFF3380', '0xCCCC00', '0x66E64D', '0x4D80CC', '0x9900B3',
        '0xE64D66', '0x4DB380', '0xFF4D4D', '0x99E6E6', '0x6666FF'];

    const spinnerContainer = new Container();
    circleContainer.addChild(spinnerContainer);

    const txtStyle = new TextStyle({
        fontSize: 36,
        fontFamily: 'Arial',
        fill: 'white'
    })

    // Spin button
    const spintTxt = new Text('Click here to spin', txtStyle);
    spinBtnContainer.x = app.view.width / 2 - spintTxt.width / 2;
    spinBtnContainer.y = 150;
    spintTxt.interactive = true;
    spintTxt.buttonMode = true;
    spintTxt.on('mousedown', spinWheel);
    spinBtnContainer.x = app.view.width / 2 - spintTxt.width / 2;
    spinBtnContainer.y = 150;
    spinBtnContainer.addChild(spintTxt);

    const metalCircle = new Sprite.from('./img/metal.png');
    const spinnerArrow = new Sprite.from('./img/arrow.png');
    metalCircle.anchor.set(0.5, 0.5);
    metalCircle.width = 70;
    metalCircle.height = 70;
    spinnerArrow.width = 32;
    spinnerArrow.height = 32;
    spinnerArrow.y = -200;
    spinnerArrow.anchor.set(0.5, 0.5);
    circleContainer.addChild(metalCircle);
    circleContainer.addChild(spinnerArrow)

    // Cuting circle to slices
    let circleSlices = apiRes.pieces;
    const circleCuts = [];
    const fullCircle = Math.PI * 2;

    for (let i = 1; i <= circleSlices; i++) {
        const txtStyle = new TextStyle({ fontFamily: 'Arial' });
        const txt = new Text(`${apiRes.values[i].value}$`, txtStyle);
        circleCuts[i] = new Graphics();
        circleCuts[i].beginFill(colorArray[Math.floor(Math.random() * colorArray.length)]);
        circleCuts[i].arc(0, 0, 200, 0, fullCircle / circleSlices);
        circleCuts[i].arcTo(0, 0, 100, 0, 1);
        txt.x = circleCuts[i].width / 2;
        circleCuts[i].addChild(txt);
        spinnerContainer.addChild(circleCuts[i]);
        if (i === 1) {
            circleCuts[i].rotation = Math.PI / circleSlices * 2 + Math.PI;
        } else {
            circleCuts[i].rotation = circleCuts[i - 1].transform.rotation + (Math.PI / circleSlices * 2)
        }
    }
    
    function spinWheel() {
        let rotCalc,
            delay = 0;

        const circleTopPositionInRadian = Math.PI / 2 + Math.PI;
        const circleCutInsideGraphic = (fullCircle / circleSlices) / 2;
        rotCalc = circleTopPositionInRadian - circleCutInsideGraphic;   

        // rotating whole wheel
        TweenMax.to(spinnerContainer, spinAnimationTime, { rotation: (Math.PI * 2) * 15, ease: Power4.easeInOut });

        // rotating circle sections
        spinnerContainer.children.forEach((circleCut, index) => {
            const removedDollarSign = circleCut.children[0].text.replace('$', '');

            if (Number(removedDollarSign) === apiRes.winningValue) {
                // winning circle section
                if (circleCut.transform.rotation > rotCalc) {
                    TweenMax.to(circleCut, spinAnimationTime, { rotation: (rotCalc + (Math.PI * 2)), ease: Power4.easeInOut })
                } else {
                    TweenMax.to(circleCut, spinAnimationTime, { rotation: rotCalc, ease: Power4.easeInOut })
                }
            } 
                const winningSectionIndex = spinnerContainer.children.findIndex(x => x.children[0].text === `${apiRes.winningValue}$`);
                if (Number(removedDollarSign) !== apiRes.winningValue) {
                    TweenMax.to(circleCut, spinAnimationTime, { rotation: rotCalc + (fullCircle / circleSlices * (index - winningSectionIndex)), ease: Power4.easeInOut })
                }
        })
    }
}